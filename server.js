const express = require("express");
const fs = require('fs');
const util = require('util');
const http = require("http");
const { Server } = require("socket.io");
const cors = require("cors");
const NotFoundError = require("./errors/not-found");
const userRouter = require("./api/users/users.router");
const articleRouter = require("./api/articles/articles.router");
const articlesController = require("./api/articles/articles.controller");
const usersController = require("./api/users/users.controller");
const authMiddleware = require("./middlewares/auth");
//require("./api/articles/articles.schema"); // temporaire
const app = express();

const server = http.createServer(app);
const io = new Server(server);

io.on("connection", (socket) => {
  console.log("a user connected");
  /*socket.on("my_event", (data) => {
    console.log(data);
  });
  io.emit("event_from_server", { test: "foo" });*/
});

app.use((req, res, next) => {
  req.io = io;
  next();
});

app.use(cors());
app.use(express.json());

app.use("/api/users", userRouter);
app.use("/api/users", authMiddleware, userRouter);
app.use("/api/articles", authMiddleware, articleRouter);

app.post("/login", usersController.login);

app.use("/", express.static("public"));

app.use((req, res, next) => {
  next(new NotFoundError());
});

const logFile = fs.createWriteStream('./logs/err.log', { flags: 'a' });

app.use((error, req, res, next) => {
  const status = error.status || 500;
  const message = error.message;
  res.status(status);
  res.json({
    status,
    message,
  });
});

let logStdout = process.stdout;

console.log = function () {
  const d = new Date();
  logFile.write(+ d.getDay() + "-"+d.getMonth()+ "-"+d.getFullYear()+ "-"+ d.getTime() + " - "+util.format.apply(null,arguments).replace(/\033\[[0-9;]*m/g,"") + '\n');
  logStdout.write(util.format.apply(null, arguments) + '\n');
}

module.exports = {
  app,
  server,
};
