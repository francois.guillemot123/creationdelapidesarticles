const express = require("express");
const usersController = require("./users.controller");
const articlesController = require("../articles/articles.controller");
const router = express.Router();
const routerFree = express.Router();


router.get("/", usersController.getAll);
router.get("/:id", usersController.getById);
router.get("/:id/articles", articlesController.getByIdUserArticles);
router.post("/", usersController.create);
router.put("/:id", usersController.update);
router.delete("/:id", usersController.delete);


module.exports = router;
