module.exports = {
  apps: [
    {
      name: "app",
      script: "./www/app.js",
      instances: 4,
      log_date_format: 'YYYY-MM-DD | HH:mm:ss | Z',
      err_file : './logs/err2.log',
      out_file: './logs/err2.log',
      //error_file: "./logs/err2.log",
      //out_file: "./logs/err.log",
      watch: true,
      max_memory_restart: '200M',
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};
