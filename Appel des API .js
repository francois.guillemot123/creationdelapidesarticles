
/* login dans mongoDB */
{
  "_id": {
    "$oid": "6372c2960e64ff1c32d8f8e6"
  },
  "name": "dqsdsqdsq",
  "password": "$2b$10$NObNAU/ZeX5fbXXMpwUxBOk9WbA3LbNlwIXSrdALsmSU8wcXVW7z6",
  "email": "estadmin@estadmin.fr",
  "role": "admin",
  "date": {
    "$date": {
      "$numberLong": "1668465302594"
    }
  },
  "__v": 0
}



/*  ---------------------------------------------------------------- */
/* Création  d'user */
/* mot de passe par défaut  "password":"azerty123",*/
import axios from "axios";

const headersList1 = {
 "Accept": "*/*",
 "User-Agent": "Thunder Client (https://www.thunderclient.com)",
 "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MzVhYjc4YWE2OGQ2MjZjMDBiNWY5MDYiLCJpYXQiOjE2Njg0NjQ0MjUsImV4cCI6MTY2ODcyMzYyNX0.cf8zGUWegM0nl5zOrWXOR15HZt60e9R4Ooa83uGo7fA",
 "Content-Type": "application/json" 
}

const bodyContent1 = JSON.stringify({
  "name":"dqsdsqdsdsqdsqdsq",
  "email":"dsdsqdsq@dddsqdsdsqddsq.fr",
  "password":"azerty123",
  "role":"member"
});

const reqOptions1 = {
  url: "http://localhost:3000/api/users/",
  method: "POST",
  headers: headersList1,
  data: bodyContent1,
}

const response1 = await axios.request(reqOptions1);
console.log(response1.data);
/*  ---------------------------------------------------------------- ----------------*/

/* Création  d'article avec TOKEN */
import axios from "axios";

const headersList2 = {
 "Accept": "*/*",
 "User-Agent": "Thunder Client (https://www.thunderclient.com)",
 "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MzcyYzQxMzZjNDMwOGMxMGNhYjY5M2QiLCJpYXQiOjE2Njg1MTYzODMsImV4cCI6MTY2ODc3NTU4M30.ZxuzncpQNy2DbUITWFncYYc8LOWO-AbzfmNsHPNAmWE",
 "Content-Type": "application/json" 
}

const bodyContent2 = JSON.stringify({
  "title":"françois guillemot isnotadmin",
  "content":"dsqdsqdsq@dsq.fr",
  "state":"published" 
});

const reqOptions2 = {
  url: "http://localhost:3000/api/articles",
  method: "POST",
  headers: headersList2,
  data: bodyContent2,
}

const response2 = await axios.request(reqOptions2);
console.log(response2.data);

/*-----------------------------------------------------------------------------------------------------------------------------------------------*/
/* ----------------------------------Création d'article avec USER= id --------------------------------------------------------------------*/
import axios from "axios";

const headersList3 = {
 "Accept": "*/*",
 "User-Agent": "Thunder Client (https://www.thunderclient.com)",
 "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MzcyYzQxMzZjNDMwOGMxMGNhYjY5M2QiLCJpYXQiOjE2Njg2MTM3ODcsImV4cCI6MTY2ODg3Mjk4N30.6YV3TtQLqQI97JYXt-HGZGuzbPgoPn4jxjjhi8wkIqk",
 "Content-Type": "application/json" 
}

const bodyContent3 = JSON.stringify({
  "title":"françois guillemot ok tactac",
  "content":"bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ",
  "state":"published",
  "user": "636f8b16f5265aa1729ea152"
});

const reqOptions3 = {
  url: "http://localhost:3000/api/articles",
  method: "POST",
  headers: headersList3,
  data: bodyContent3,
}

const response3 = await axios.request(reqOptions3);
console.log(response3.data);

/* ---------------------------------------------------Update d'article --------------------------------------------------- */


import axios from "axios";

const headersList4 = {
 "Accept": "*/*",
 "User-Agent": "Thunder Client (https://www.thunderclient.com)",
 "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MzcyYzM5YjBlNjRmZjFjMzJkOGY4ZWMiLCJpYXQiOjE2Njg2MTQwNjksImV4cCI6MTY2ODg3MzI2OX0.k8TuS7MPmHRcgOMLYpg_4LAaJogAvuyoltUZ72ur8-s",
 "Content-Type": "application/json" 
}

const bodyConten4 = JSON.stringify({
  "title":"texte mis à jdsqdsqdsqdsqour par François bla bla bla bla",
  "content":"Autre bla bla bla bla bla",
  "state":"published",
  "user": "636f8b16f5265aa1729ea152"
});

const reqOptions4 = {
  url: "http://localhost:3000/api/articles/6372bf71a33989ea08eefdc4",
  method: "PUT",
  headers: headersList4,
  data: bodyConten4,
}

const response4 = await axios.request(reqOptions4);
console.log(response4.data);


/* ---------------------------------------------------suppression ----------------------------------*/
import axios from "axios";

const headersList5 = {
 "Accept": "*/*",
 "User-Agent": "Thunder Client (https://www.thunderclient.com)",
 "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MzcyYzM5YjBlNjRmZjFjMzJkOGY4ZWMiLCJpYXQiOjE2Njg2MTQwNjksImV4cCI6MTY2ODg3MzI2OX0.k8TuS7MPmHRcgOMLYpg_4LAaJogAvuyoltUZ72ur8-s",
 "Content-Type": "application/json" 
}

const bodyContent5 = JSON.stringify({
  "title":"françois guillemot texte",
  "content":"c'est un beau roman c'est une belle histoire",
  "state":"draft",
  "user": "636f8b16f5265aa1729ea152"
});

const reqOptions5 = {
  url: "http://localhost:3000/api/articles/6372bf37a33989ea08eefdbe",
  method: "DELETE",
  headers: headersList5,
  data: bodyContent5,
}

const response5 = await axios.request(reqOptions5);
console.log(response5.data);


/* -------------------------------------------------------------------   */
/* lecture des article d'un USER en mode déconnecté    ----------------- */

import axios from "axios";

const headersList6 = {
 "Accept": "*/*",
 "User-Agent": "Thunder Client (https://www.thunderclient.com)" 
}

const reqOptions6 = {
  url: "http://localhost:3000/api/users/6372c39b0e64ff1c32d8f8ec/articles",
  method: "GET",
  headers: headersList6,
}

const response6 = await axios.request(reqOptions6);
console.log(response6.data);
