//const User = require("../../api/users/users.model");
const Article = require("./articles.schema");


class ArticlesService {
    /*getAll() {
      return Article.find({}, "-password");
    }*/
    getUserAticles(id) {
      const article = Article.find({user: id, state : 'published'});  
      console.log(article);
      return article;
    } 

    create(data) {
      const article = new Article(data);
      return article.save();
    }
    update(id, data) {
      return Article.findByIdAndUpdate(id, data, { new: true });
    }

    delete(id) {
      return Article.deleteOne({ _id: id });
    }
    
  }
  
  module.exports = new ArticlesService();