const request = require("supertest");
const { app } = require("../server");
const jwt = require("jsonwebtoken");
const config = require("../config");
const mongoose = require("mongoose");
const mockingoose = require("mockingoose");
const Article = require("../api/articles/articles.schema");
const User = require("../api/users/users.model");
const articlesService = require("../api/articles/articles.service");
const usersService = require("../api/users/users.service");


describe("tester API articles", () => {
  let token;
 
  const ARTICLE_ID = "fake";
  const USER_ID =  "fake";


  const MOCK_DATA = [
    {
      ObjectId: USER_ID,
      name: "ana",
      email: "nfegeg@gmail.com",
      password: "azertyuiop",
      role:"member",
    },
  ];
const UpdateArticle =  
  {
    ObjectId: ARTICLE_ID,
    title:"françois guillemot isnotadmin",
    content:"<p style='text-align:justify'><span style='font-size:12px'><span style='font-family:Lucida Sans Unicode,Lucida Grande,sans-serif'><strong>Résolution&nbsp;:</strong><br>Toute inexécution par notre client de l’une de ses obligations entrainera la résolution de plein droit non seulement de la commande en cause mais également de toutes les commandes impayées par ledit client  qu’elles soient livrées ou en cours de livraison et que le paiement soit échu ou non, le tout sans mise en demeure préalable. Il en va de même si par son action ou son omission notre client rend plus difficile l’exercice du droit de revendication dont LA BS est titulaire en application de la loi. Les présentes conditions générales de vente sont systématiquement jointes à nos devis et factures.<br><br>",
    state:"published" ,
    user:MOCK_DATA.ObjectId,
  }
 ;

const article = 
  {
    _id: ARTICLE_ID,
    title:"françois guillemot isnotadmin",
    content:"<p style='text-align:justify'><span style='font-size:12px'><span style='font-family:Lucida Sans Unicode,Lucida Grande,sans-serif'><strong>Résolution&nbsp;:</strong><br>Toute inexécution par notre client de l’une de ses obligations entrainera la résolution de plein droit non seulement de la commande en cause mais également de toutes les commandes impayées par ledit client  qu’elles soient livrées ou en cours de livraison et que le paiement soit échu ou non, le tout sans mise en demeure préalable. Il en va de même si par son action ou son omission notre client rend plus difficile l’exercice du droit de revendication dont LA BS est titulaire en application de la loi. Les présentes conditions générales de vente sont systématiquement jointes à nos devis et factures.<br><br>",
    state:"draft",
  user:MOCK_DATA.ObjectId,
  
  }
;
 
 const cleId = ARTICLE_ID; //'6372bf37a33989ea08eefdbe';
 
  beforeEach(() => {
    //là je ne sais pas trop comment passer le token les tests passent mais c'est erreur 500 USER_ID ?
    token = jwt.sign({ userId: USER_ID }, config.secretJwtToken);
   // console.log(token);
    mockingoose(User).toReturn(MOCK_DATA, "find");
    mockingoose(Article).toReturn(article, "create");
    mockingoose(Article).toReturn(UpdateArticle, "update");
    mockingoose(Article).toReturn(UpdateArticle, "delete");
    
  });

 
  test('Login test ? ', async () => {

    try {
      const res = await request(app).post('/login'); 
    } catch (err) {
      console.log(`Error ${err}`)
    }
  });

  test('Create an article', async() => {
  
    try {
        const res  = await request(app).post('/api/articles')
        .send(article)
        .set("x-access-token", token); 
        expect(res.status).toBe(201);
        expect(res.body.title).toBe(article.title);
     
    } catch (err) {
        console.log(`Error ${err}`)
    }
});

test('Delete an article', async() => {

  try {
    const res = await request(app).delete('/api/articles/'+cleId).set("x-access-token", token)
    .send(cleId)
    .set("x-access-token", token); 
    //expect(res.body.length).toBeGreaterThan(0);
    expect(res.status).toBe(204);

     
  } catch (err) {
      console.log(`Error ${err}`)
  }
});

test('Update an article', async() => {
  try {
      const res = await request(app).put('/api/articles/')
      .send(UpdateArticle)
      .set("x-access-token", token);  
       
      expect(res.body.title).toBe(UpdateArticle.title);
      expect(res.status).toBe(200);
  } catch (err) {
      console.log(`Error ${err}`)
  }
});

afterEach(() => {
  jest.restoreAllMocks();
});
/*
  test("Mock", async () => {
    const myFunction = jest.fn().mockResolvedValue("test");
    const val = await myFunction();
    console.log(val);
    expect(val).toBe("test");
  });
  //test d'espion 
  test("Spy article service getall espion", async () => {
    const spy = jest.spyOn(articlesService, "getAll")
    .mockImplementation(() => "test");
    await request(app).get("/api/articles").set("x-access-token", token);
    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveReturnedWith("test");
  });
  //restauration des Mocks
  afterEach(() => {
    jest.restoreAllMocks();
  });
  */

});
