const NotFoundError = require("../../errors/not-found");
const UnauthorizedError = require("../../errors/unauthorized");
const jwt = require("jsonwebtoken");
const config = require("../../config");
const ArticlesService = require("./articles.service");
const usersService = require("../users/users.service");
const Article = require("./articles.schema");

class articlesController {

   async getByIdUserArticles(req, res, next) {
      try {
        const id = req.params.id;
        console.log(id);
        const article = await ArticlesService.getUserAticles(id);
        if (!article) {
          throw new NotFoundError();
       }
       req.io.emit("articles:read", article);
       res.status(201).json(article);
      } catch (err) {
        next(err);
      }
    }
    
  async create(req, res, next) {
    try {

      const token = await req.headers["x-access-token"];
      if (!token) {
        throw new UnauthorizedError();
      }
      const decoded = await jwt.verify(token, config.secretJwtToken);
      
      const newArticle =  {
        title: req.body.title,
        content: req.body.content,
        state:req.body.state,
        user: decoded.userId,
      };
      console.log(req.body);
      console.log(newArticle);
      const article = await ArticlesService.create(newArticle);
      //article.password = undefined;
      req.io.emit("article:create", article);
      res.status(201).json(article);
    } catch (err) {
      next(err);
    }
  }

  async update(req, res, next) {
    try {
      const token = await req.headers["x-access-token"];
      if (!token) {
        throw new UnauthorizedError();
      }
      const decoded = await jwt.verify(token, config.secretJwtToken);
      //console.log(decoded.userId);
      const rule = await usersService.getRole(decoded.userId);

      if (rule == "admin") {
        const id = req.params.id;
        const data = req.body;
        const articleModified = await ArticlesService.update(id, data);
        articleModified.password = undefined;
        res.status(201).json(articleModified);
      }
      else {
        throw new UnauthorizedError();
      }
    } catch (err) {
      next(err);
    }
  }
  
  async delete(req, res, next) {
    try {
      const token = await req.headers["x-access-token"];
      if (!token) {
        throw new UnauthorizedError();
      }
      const decoded = await jwt.verify(token, config.secretJwtToken);
      // console.log(decoded.userId);
      const Role = await usersService.getRole(decoded.userId);
      // console.log(Role);
      if (Role == "admin") {
        const id = req.params.id;
        await ArticlesService.delete(id);
        req.io.emit("article:delete", { id });
        res.status(204).send();
      }
      else {
        throw new UnauthorizedError();
      }
    } catch (err) {
      next(err);
    }
  }
   
}

module.exports = new articlesController();

 /*
  async getAll(req, res, next) {
    try {
      const Articles = await ArticlesService.getAll();
      res.json(Articles);
    } catch (err) {
      next(err);
    }
  } */